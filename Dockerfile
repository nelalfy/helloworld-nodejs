FROM registry.access.redhat.com/ubi9/nodejs-18-minimal:latest

WORKDIR /app

COPY package*.json ./

USER root

RUN chmod -R 777 /app 

RUN npm install 

COPY . .

EXPOSE 3000

USER 1001

CMD [ "node", "app.js" ]